import 'package:bookdiscoverytestapp/bloc/authentication/authentication.dart';
import 'package:bookdiscoverytestapp/bloc/base_blocs.dart';
import 'package:bookdiscoverytestapp/data/model/data_mappers.dart';
import 'package:bookdiscoverytestapp/data/model/error/exceptions.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ErrorBlocObserver extends BlocObserver {
  @override
  void onEvent(Bloc bloc, Object? event) {
    print('BLOC Event: $event');
    super.onEvent(bloc, event);
  }

  @override
  void onTransition(Bloc bloc, Transition transition) {
    print('BLOC Transition: $transition');
    super.onTransition(bloc, transition);
  }

  @override
  void onError(BlocBase bloc, Object error, StackTrace stackTrace) {
    print('BLOC Error: $error');

    final Exception? exception = DataMapper.dynamicErrorToTyped(error);
    if (exception is UnAuthException) {
      if (bloc is BaseNetworkBloc) {
        BlocProvider.of<AuthenticationBloc>(bloc.context)
            .add(AuthenticationLogoutEvent());
      }
    }

    super.onError(bloc, error, stackTrace);
  }
}
