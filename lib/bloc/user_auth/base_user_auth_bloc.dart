import 'dart:async';

import 'package:bookdiscoverytestapp/bloc/authentication/authentication.dart';
import 'package:bookdiscoverytestapp/bloc/base_blocs.dart';
import 'package:bookdiscoverytestapp/bloc/user_auth/user_auth.dart';
import 'package:bookdiscoverytestapp/data/model/data_mappers.dart';
import 'package:bookdiscoverytestapp/data/model/error/exceptions.dart';
import 'package:bookdiscoverytestapp/data/model/network/dto_models.dart';
import 'package:bookdiscoverytestapp/widget/context/data_context.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

typedef UserAuthCall = Future<UserDto> Function(LoginDto);

abstract class BaseUserUserAuthBloc
    extends BaseNetworkBloc<UserAuthEvent, UserAuthState> {
  BaseUserUserAuthBloc({
    required BuildContext context,
    required DataContext dataContext,
  })  : authenticationBloc = BlocProvider.of<AuthenticationBloc>(context),
        super(
          UserAuthInitialState(),
          dataContext: dataContext,
          context: context,
        ) {
    on<UserAuthEvent>(_onUserAuthEvent);
  }

  final AuthenticationBloc authenticationBloc;

  FutureOr<void> _onUserAuthEvent(
    UserAuthEvent event,
    Emitter<UserAuthState> emit,
  ) async {
    emit(UserAuthInProgressState());
    try {
      final UserDto userDto = await userAuthCall.call(
        LoginDto(
          event.username,
          event.password,
        ),
      );

      authenticationBloc.add(AuthenticationLoginEvent(userDto.token));
    } catch (error) {
      late final String? message;
      final Exception? exception = DataMapper.dynamicErrorToTyped(error);
      if (exception is MessageException) {
        message = exception.message;
      } else {
        message = null;
      }

      emit(UserAuthFailureState(message));
    }
  }

  UserAuthCall get userAuthCall;
}
