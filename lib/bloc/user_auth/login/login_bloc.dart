import 'package:bookdiscoverytestapp/bloc/user_auth/base_user_auth_bloc.dart';
import 'package:bookdiscoverytestapp/widget/context/data_context.dart';
import 'package:flutter/material.dart';

class LoginBloc extends BaseUserUserAuthBloc {
  LoginBloc({required BuildContext context, required DataContext dataContext})
      : super(context: context, dataContext: dataContext);

  @override
  UserAuthCall get userAuthCall => dataSource.login;
}
