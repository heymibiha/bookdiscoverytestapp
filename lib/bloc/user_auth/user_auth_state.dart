abstract class UserAuthState {
  const UserAuthState();
}

class UserAuthInitialState extends UserAuthState {}

class UserAuthInProgressState extends UserAuthState {}

class UserAuthFailureState extends UserAuthState {
  UserAuthFailureState(this.message);

  final String? message;
}
