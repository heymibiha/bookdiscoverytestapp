class UserAuthEvent {
  const UserAuthEvent({
    required this.username,
    required this.password,
  });

  final String username;
  final String password;
}
