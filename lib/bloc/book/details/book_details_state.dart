import 'package:bookdiscoverytestapp/data/model/domain/domain_models.dart';
import 'package:equatable/equatable.dart';

abstract class BookDetailsState extends Equatable {
  const BookDetailsState();

  @override
  List<Object?> get props => [];
}

class BookDetailsInitial extends BookDetailsState {}

class BookDetailsLoading extends BookDetailsState {}

class BookDetailsFailure extends BookDetailsState {}

class BookDetailsSuccess extends BookDetailsState {
  const BookDetailsSuccess(this.book);

  final BookDomain book;

  @override
  List<Object?> get props => [book.id];
}
