import 'package:equatable/equatable.dart';

class BookDetailsEvent extends Equatable {
  const BookDetailsEvent(this.id);

  final String id;

  @override
  List<Object?> get props => [id];
}
