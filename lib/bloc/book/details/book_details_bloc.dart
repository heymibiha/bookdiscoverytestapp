import 'dart:async';

import 'package:bookdiscoverytestapp/bloc/base_blocs.dart';
import 'package:bookdiscoverytestapp/bloc/book/details/book_details.dart';
import 'package:bookdiscoverytestapp/data/model/data_mappers.dart';
import 'package:bookdiscoverytestapp/data/model/domain/domain_models.dart';
import 'package:bookdiscoverytestapp/data/model/network/dto_models.dart';
import 'package:bookdiscoverytestapp/widget/context/data_context.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class BookDetailsBloc extends BaseNetworkBloc<BookDetailsEvent, BookDetailsState> {
  BookDetailsBloc({
    required BuildContext context,
    required DataContext dataContext,
  }) : super(
          BookDetailsInitial(),
          dataContext: dataContext,
          context: context,
        ) {
    on<BookDetailsEvent>(_onBookDetailsEvent);
  }

  Future<FutureOr<void>> _onBookDetailsEvent(
      BookDetailsEvent event, Emitter<BookDetailsState> emit,) async {
    try {
      emit(BookDetailsLoading());

      final BookDto dto = await dataSource.book(event.id);
      final BookDomain domain = DataMapper.convertBookDtoToDomain(dto);

      emit(BookDetailsSuccess(domain));
    } catch (error) {
      emit(BookDetailsFailure());
      rethrow;
    }
  }
}
