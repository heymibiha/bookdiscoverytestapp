import 'dart:async';

import 'package:bookdiscoverytestapp/bloc/base_blocs.dart';
import 'package:bookdiscoverytestapp/bloc/book/list/book_list.dart';
import 'package:bookdiscoverytestapp/data/model/data_mappers.dart';
import 'package:bookdiscoverytestapp/data/model/domain/domain_models.dart';
import 'package:bookdiscoverytestapp/data/model/network/dto_models.dart';
import 'package:bookdiscoverytestapp/widget/context/data_context.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:rxdart/rxdart.dart';

class BookListBloc extends BaseNetworkBloc<BookListEvent, BookListState> {
  BookListBloc({
    required BuildContext context,
    required DataContext dataContext,
  }) : super(
          BookListInitial(),
          dataContext: dataContext,
          context: context,
        ) {
    on<BookListEvent>(
      _onBookListEvent,
      transformer: _debounceTransformer(const Duration(milliseconds: 300)),
    );
  }

  static EventTransformer<Event> _debounceTransformer<Event>(
    Duration duration,
  ) {
    return (events, mapper) {
      return events.debounceTime(duration).switchMap(mapper);
    };
  }

  Future<FutureOr<void>> _onBookListEvent(
    BookListEvent event,
    Emitter<BookListState> emit,
  ) async {
    try {
      emit(BookListLoading());

      final List<BookDto> dtoList = await dataSource.books(event.search);
      final List<BookListItemDomain> domainList = dtoList
          .map((dto) => DataMapper.convertBookDtoToListItemDomain(dto))
          .toList();

      emit(BookListSuccess(domainList));
    } catch (error) {
      emit(BookListFailure());
      rethrow;
    }
  }
}
