import 'package:equatable/equatable.dart';

class BookListEvent extends Equatable {
  const BookListEvent(this.search);

  final String search;

  @override
  List<Object?> get props => [search];
}
