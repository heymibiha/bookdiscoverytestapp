import 'package:bookdiscoverytestapp/data/model/domain/domain_models.dart';
import 'package:equatable/equatable.dart';

abstract class BookListState extends Equatable {
  const BookListState();

  @override
  List<Object?> get props => [];
}

class BookListInitial extends BookListState {}

class BookListLoading extends BookListState {}

class BookListFailure extends BookListState {}

class BookListSuccess extends BookListState {
  const BookListSuccess(this.items);

  final List<BookListItemDomain> items;

  @override
  List<Object?> get props => [items];
}
