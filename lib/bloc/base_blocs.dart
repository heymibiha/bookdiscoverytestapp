import 'package:bloc/bloc.dart';
import 'package:bookdiscoverytestapp/data/source/data_source.dart';
import 'package:bookdiscoverytestapp/widget/context/data_context.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

abstract class BaseNetworkBloc<Event, State> extends Bloc<Event, State> {
  BaseNetworkBloc(
    State initialState, {
    required this.dataContext,
    required this.context,
  }) : super(initialState);

  final DataContext dataContext;
  final BuildContext context;

  DataSource get dataSource => dataContext.dataSource;
}
