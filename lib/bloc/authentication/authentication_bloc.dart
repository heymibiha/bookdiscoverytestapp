import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:bookdiscoverytestapp/bloc/authentication/authentication.dart';
import 'package:bookdiscoverytestapp/data/source/rest/user_repository.dart';

class AuthenticationBloc
    extends Bloc<AuthenticationEvent, AuthenticationState> {
  AuthenticationBloc(this.userRepository)
      : super(AuthenticationInitialState()) {
    on<AuthenticationStartEvent>(_onAuthenticationStartEvent);
    on<AuthenticationLoginEvent>(_onAuthenticationLoginEvent);
    on<AuthenticationLogoutEvent>(_onAuthenticationLogoutEvent);
  }

  final UserRepository userRepository;

  FutureOr<void> _onAuthenticationStartEvent(
    AuthenticationStartEvent event,
    Emitter<AuthenticationState> emit,
  ) async {
    final String? token = await userRepository.loadToken();

    if (token?.isEmpty ?? true) {
      emit(AuthenticationFailureState());
    } else {
      emit(AuthenticationSuccessState());
    }
  }

  FutureOr<void> _onAuthenticationLoginEvent(
    AuthenticationLoginEvent event,
    Emitter<AuthenticationState> emit,
  ) {
    emit(AuthenticationInProgressState());
    userRepository.saveToken(event.token);
    emit(AuthenticationSuccessState());
  }

  FutureOr<void> _onAuthenticationLogoutEvent(
    AuthenticationLogoutEvent event,
    Emitter<AuthenticationState> emit,
  ) {
    userRepository.deleteToken();
    emit(AuthenticationFailureState());
  }
}
