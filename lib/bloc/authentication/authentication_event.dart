import 'package:equatable/equatable.dart';

abstract class AuthenticationEvent extends Equatable{
  const AuthenticationEvent();

  @override
  List<Object?> get props => [];
}

class AuthenticationStartEvent extends AuthenticationEvent {}

class AuthenticationLoginEvent extends AuthenticationEvent {
  const AuthenticationLoginEvent(this.token);

  final String token;

  @override
  List<Object?> get props => [token];
}

class AuthenticationLogoutEvent extends AuthenticationEvent {}
