import 'package:bookdiscoverytestapp/bloc/authentication/authentication.dart';
import 'package:bookdiscoverytestapp/bloc/bloc_observers.dart';
import 'package:bookdiscoverytestapp/data/source/rest/http_client.dart';
import 'package:bookdiscoverytestapp/data/source/rest/rest_data_source.dart';
import 'package:bookdiscoverytestapp/data/source/rest/user_repository.dart';
import 'package:bookdiscoverytestapp/widget/context/data_context.dart';
import 'package:bookdiscoverytestapp/widget/pages/book/book_list_page.dart';
import 'package:bookdiscoverytestapp/widget/pages/splash_page.dart';
import 'package:bookdiscoverytestapp/widget/pages/user_auth/login_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

void main() {
  BlocOverrides.runZoned(
    () => runApp(MyApp()),
    blocObserver: ErrorBlocObserver(),
  );
}

class MyApp extends StatelessWidget {
  final HttpClient _httpClient = HttpClient();
  late final UserRepository _userRepository = UserRepository(
    tokenChangeCallback: (token) => _httpClient.refreshToken = token,
  );
  late final AuthenticationBloc _authenticationBloc =
      AuthenticationBloc(_userRepository);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      debugShowCheckedModeBanner: false,
      onGenerateRoute: (_) =>
          MaterialPageRoute(builder: (_) => const SplashPage()),
      builder: (BuildContext context, Widget? child) {
        GlobalKey<NavigatorState>? navigatorKey;
        if (child?.key is GlobalKey<NavigatorState>) {
          navigatorKey = child!.key as GlobalKey<NavigatorState>?;
        }

        return BlocProvider(
          create: (_) => _authenticationBloc,
          child: _buildAuthListener(
            navigatorKey: navigatorKey,
            child: child,
          ),
        );
      },
    );
  }

  Widget _buildAuthListener({
    GlobalKey<NavigatorState>? navigatorKey,
    Widget? child,
  }) {
    return BlocListener<AuthenticationBloc, AuthenticationState>(
      listener: (BuildContext context, AuthenticationState state) {
        Widget? homepage;

        if (state is AuthenticationSuccessState) {
          homepage = const BookListPage();
        } else if (state is AuthenticationFailureState) {
          _clearNavigation(navigatorKey);
          homepage = const LoginPage();
        } else if (state is AuthenticationInProgressState) {
          homepage = const Center(child: CircularProgressIndicator());
        }

        if (homepage != null && navigatorKey != null) {
          final Route route = MaterialPageRoute(builder: (_) => homepage!);
          navigatorKey.currentState?.pushReplacement(route);
        }
      },
      child: _buildDataContext(
        child: child,
      ),
    );
  }

  static void _clearNavigation(GlobalKey<NavigatorState>? navigatorKey) {
    if (navigatorKey?.currentState?.canPop() ?? false) {
      navigatorKey!.currentState!.popUntil((route) => route.isFirst);
    }
  }

  Widget? _buildDataContext({Widget? child}) {
    if (child == null) {
      return null;
    }

    return DataContext(
      dataSource: RestDataSource(_httpClient),
      child: child,
    );
  }
}
