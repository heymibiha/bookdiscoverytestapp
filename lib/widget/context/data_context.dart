import 'package:bookdiscoverytestapp/data/source/data_source.dart';
import 'package:flutter/material.dart';

class DataContext extends InheritedWidget {
  const DataContext({required this.dataSource, required Widget child})
      : super(child: child);

  final DataSource dataSource;

  static DataContext of(BuildContext context) {
    final DataContext? result =
        context.dependOnInheritedWidgetOfExactType<DataContext>();
    assert(result != null, 'No DataContext found in context');
    return result!;
  }

  @override
  bool updateShouldNotify(DataContext oldWidget) =>
      dataSource.runtimeType != oldWidget.runtimeType;
}
