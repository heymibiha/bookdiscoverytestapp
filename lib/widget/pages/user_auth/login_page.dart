import 'package:bookdiscoverytestapp/bloc/user_auth/login/login_bloc.dart';
import 'package:bookdiscoverytestapp/bloc/user_auth/user_auth_event.dart';
import 'package:bookdiscoverytestapp/bloc/user_auth/user_auth_state.dart';
import 'package:bookdiscoverytestapp/widget/context/data_context.dart';
import 'package:bookdiscoverytestapp/widget/controls/auth_control.dart';
import 'package:bookdiscoverytestapp/widget/pages/user_auth/register_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class LoginPage extends StatelessWidget {
  const LoginPage();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 30),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              _buildBlocProvider(context: context),
              _buildRegisterButton(context: context),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildAuthControl() {
    return BlocListener<LoginBloc, UserAuthState>(
      listener: (context, state) async {
        if (state is UserAuthFailureState) {
          ScaffoldMessenger.of(context)
            ..hideCurrentSnackBar()
            ..showSnackBar(
              SnackBar(content: Text(state.message ?? 'Login Failed')),
            );
        }
      },
      child: BlocBuilder<LoginBloc, UserAuthState>(
        builder: (context, state) {
          return AuthControl(
            onSubmit: (username, password) => _onLoginSubmit(
              context,
              username: username,
              password: password,
            ),
            submitButtonName: 'LOGIN',
            state: state,
          );
        },
      ),
    );
  }

  void _onLoginSubmit(
    BuildContext context, {
    required String username,
    required String password,
  }) {
    BlocProvider.of<LoginBloc>(context).add(
      UserAuthEvent(
        username: username,
        password: password,
      ),
    );
  }

  Widget _buildBlocProvider({required BuildContext context}) {
    final DataContext dataContext = DataContext.of(context);

    return BlocProvider(
      create: (context) => LoginBloc(
        dataContext: dataContext,
        context: context,
      ),
      child: _buildAuthControl(),
    );
  }

  Widget _buildRegisterButton({required BuildContext context}) {
    return ElevatedButton(
      onPressed: () => _onRegisterButtonPressed(context: context),
      child: const Text('REGISTRATION'),
    );
  }

  void _onRegisterButtonPressed({required BuildContext context}) {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (_) => const RegisterPage()),
    );
  }
}
