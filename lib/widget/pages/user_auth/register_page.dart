import 'package:bookdiscoverytestapp/bloc/user_auth/register/register_bloc.dart';
import 'package:bookdiscoverytestapp/bloc/user_auth/user_auth_event.dart';
import 'package:bookdiscoverytestapp/bloc/user_auth/user_auth_state.dart';
import 'package:bookdiscoverytestapp/widget/context/data_context.dart';
import 'package:bookdiscoverytestapp/widget/controls/auth_control.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class RegisterPage extends StatelessWidget {
  const RegisterPage();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Registration'),
      ),
      body: Center(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 30),
          child: _buildBlocProvider(context: context),
        ),
      ),
    );
  }

  Widget _buildAuthControl() {
    return BlocListener<RegisterBloc, UserAuthState>(
      listener: (context, state) async {
        if (state is UserAuthFailureState) {
          ScaffoldMessenger.of(context)
            ..hideCurrentSnackBar()
            ..showSnackBar(
              SnackBar(content: Text(state.message ?? 'Registration Failed')),
            );
        }
      },
      child: BlocBuilder<RegisterBloc, UserAuthState>(
        builder: (context, state) {
          return AuthControl(
            onSubmit: (username, password) => _onLoginSubmit(
              context,
              username: username,
              password: password,
            ),
            submitButtonName: 'REGISTER',
            state: state,
          );
        },
      ),
    );
  }

  void _onLoginSubmit(
    BuildContext context, {
    required String username,
    required String password,
  }) {
    BlocProvider.of<RegisterBloc>(context).add(
      UserAuthEvent(
        username: username,
        password: password,
      ),
    );
  }

  Widget _buildBlocProvider({required BuildContext context}) {
    final DataContext dataContext = DataContext.of(context);

    return BlocProvider(
      create: (context) => RegisterBloc(
        dataContext: dataContext,
        context: context,
      ),
      child: _buildAuthControl(),
    );
  }
}
