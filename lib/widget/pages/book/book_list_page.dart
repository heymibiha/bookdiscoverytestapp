import 'package:bookdiscoverytestapp/bloc/authentication/authentication.dart';
import 'package:bookdiscoverytestapp/bloc/book/list/book_list.dart';
import 'package:bookdiscoverytestapp/data/model/domain/domain_models.dart';
import 'package:bookdiscoverytestapp/widget/context/data_context.dart';
import 'package:bookdiscoverytestapp/widget/pages/book/book_details_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class BookListPage extends StatelessWidget {
  const BookListPage();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Books'),
        actions: [_buildLogoutAction(context: context)],
      ),
      body: _buildBlocProvider(context: context),
    );
  }

  Widget _buildBlocProvider({required BuildContext context}) {
    final DataContext dataContext = DataContext.of(context);

    return BlocProvider(
      create: (context) => BookListBloc(
        dataContext: dataContext,
        context: context,
      )..add(const BookListEvent('')),
      child: BlocBuilder<BookListBloc, BookListState>(
        builder: (context, state) {
          return _BookListWidget(
            state: state,
            searchCallback: (search) => _onBookSearch(context, search),
          );
        },
      ),
    );
  }

  Widget _buildLogoutAction({required BuildContext context}) {
    return IconButton(
      onPressed: () => _onLogoutPressed(context: context),
      icon: const Icon(Icons.logout),
    );
  }

  void _onLogoutPressed({required BuildContext context}) {
    BlocProvider.of<AuthenticationBloc>(context)
        .add(AuthenticationLogoutEvent());
  }

  void _onBookSearch(BuildContext context, String search) {
    BlocProvider.of<BookListBloc>(context).add(
      BookListEvent(search),
    );
  }
}

typedef SearchCallback = void Function(String);

class _BookListWidget extends StatefulWidget {
  const _BookListWidget({
    this.searchCallback,
    required this.state,
  });

  final SearchCallback? searchCallback;
  final BookListState state;

  @override
  State<_BookListWidget> createState() => _BookListWidgetState();
}

class _BookListWidgetState extends State<_BookListWidget> {
  final TextEditingController _searchTextController = TextEditingController();

  @override
  void initState() {
    _searchTextController.addListener(() {
      widget.searchCallback?.call(_searchTextController.text);
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20),
      child: Column(
        children: [
          _buildSearchBar(),
          Expanded(child: _buildContent()),
        ],
      ),
    );
  }

  Widget _buildSearchBar() {
    return TextField(
      controller: _searchTextController,
      decoration: const InputDecoration(
        hintText: 'Search',
      ),
    );
  }

  Widget _buildContent() {
    final BookListState state = widget.state;

    late final Widget content;

    if (state is BookListSuccess) {
      content = _buildList(state.items);
    } else {
      late final Widget child;
      if (state is BookListFailure) {
        child = const Text('Error Loading Data');
      } else {
        child = const CircularProgressIndicator();
      }

      content = Center(child: child);
    }

    return content;
  }

  Widget _buildList(List<BookListItemDomain> items) {
    return ListView.builder(
      itemBuilder: (BuildContext context, int index) {
        return _BookListItemWidget(
          item: items[index],
          onTap: () => _onListItemPressed(items[index], context),
        );
      },
      itemCount: items.length,
    );
  }

  void _onListItemPressed(BookListItemDomain item, BuildContext context) {
    Navigator.of(context)
        .push(MaterialPageRoute(builder: (_) => BookDetailsPage(id: item.id)));
  }

  @override
  void dispose() {
    _searchTextController.dispose();
    super.dispose();
  }
}

class _BookListItemWidget extends StatelessWidget {
  const _BookListItemWidget({
    Key? key,
    required this.item,
    this.onTap,
  }) : super(key: key);

  final BookListItemDomain item;
  final VoidCallback? onTap;

  static const double COVER_IMAGE_WIDTH = 50;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Card(
        child: SizedBox(
          height: 120,
          child: Row(
            children: [
              const SizedBox(width: 15),
              _buildCoverImage(),
              const SizedBox(width: 15),
              Expanded(child: _buildTitle()),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildCoverImage() {
    return Image.network(
      item.coverImageUrl ?? '',
      width: COVER_IMAGE_WIDTH,
      fit: BoxFit.fill,
      errorBuilder: (context, error, stackTrace) {
        return const SizedBox(width: COVER_IMAGE_WIDTH); //do something
      },
    );
  }

  Widget _buildTitle() {
    return Text(
      item.title,
      maxLines: 3,
      overflow: TextOverflow.ellipsis,
    );
  }
}
