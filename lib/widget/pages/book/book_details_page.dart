import 'package:bookdiscoverytestapp/bloc/book/details/book_details.dart';
import 'package:bookdiscoverytestapp/data/model/domain/domain_models.dart';
import 'package:bookdiscoverytestapp/widget/context/data_context.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class BookDetailsPage extends StatelessWidget {
  const BookDetailsPage({Key? key, required this.id}) : super(key: key);

  final String id;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Book'),
      ),
      body: _buildBlocProvider(context: context),
    );
  }

  Widget _buildBlocProvider({required BuildContext context}) {
    final DataContext dataContext = DataContext.of(context);

    return BlocProvider(
      create: (context) => BookDetailsBloc(
        dataContext: dataContext,
        context: context,
      )..add(BookDetailsEvent(id)),
      child: BlocBuilder<BookDetailsBloc, BookDetailsState>(
        builder: (context, state) {
          return _BookDetailsWidget(state: state);
        },
      ),
    );
  }
}

class _BookDetailsWidget extends StatelessWidget {
  const _BookDetailsWidget({
    required this.state,
  });

  final BookDetailsState state;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20),
      child: _buildContent(),
    );
  }

  Widget _buildContent() {
    final BookDetailsState localState = state;
    late final Widget content;

    if (localState is BookDetailsSuccess) {
      content = _buildInfo(localState.book);
    } else {
      late final Widget child;
      if (localState is BookDetailsFailure) {
        child = const Text('Error Loading Data');
      } else {
        child = const CircularProgressIndicator();
      }

      content = Center(child: child);
    }

    return content;
  }

  Widget _buildInfo(BookDomain book) {
    final Map<String, String> descriptionMap = _prepareDescriptionMap(book);
    final List<Widget> descriptions = [];
    for (final List<Widget> list
        in descriptionMap.entries.map(_buildDescriptionEntry)) {
      descriptions.addAll(list);
    }

    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const SizedBox(height: 20),
          _buildCoverImage(book),
          ...descriptions,
        ],
      ),
    );
  }

  Widget _buildCoverImage(BookDomain book) {
    return Center(
      child: Image.network(
        book.coverImageUrl ?? '',
        height: 250,
        fit: BoxFit.fitHeight,
        errorBuilder: (context, error, stackTrace) {
          return const SizedBox(); //do something
        },
      ),
    );
  }

  static Map<String, String> _prepareDescriptionMap(BookDomain book) {
    final Map<String, String> map = {};
    _addToMapIfValueIsNotEmpty(map, 'Title', book.title);
    _addToMapIfValueIsNotEmpty(map, 'Author', book.author);
    _addToMapIfValueIsNotEmpty(map, 'Page Count', '${book.pageCount}');
    _addToMapIfValueIsNotEmpty(map, 'Publisher', book.publisher);
    _addToMapIfValueIsNotEmpty(map, 'Synopsis', book.synopsis);

    return map;
  }

  static void _addToMapIfValueIsNotEmpty(
    Map<String, String> map,
    String key,
    String? value,
  ) {
    if (value?.trim().isNotEmpty ?? false) {
      map[key] = value!;
    }
  }

  List<Widget> _buildDescriptionEntry(MapEntry<String, String> mapEntry) {
    return [
      const SizedBox(height: 20),
      Text(
        '${mapEntry.key}:',
        style: const TextStyle(fontWeight: FontWeight.bold),
      ),
      Text(mapEntry.value),
    ];
  }
}
