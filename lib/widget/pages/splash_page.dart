import 'dart:async';

import 'package:bookdiscoverytestapp/bloc/authentication/authentication.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class SplashPage extends StatelessWidget {
  const SplashPage();

  @override
  Widget build(BuildContext context) {
    Future.delayed(
      const Duration(milliseconds: 2000),
      () => _onLoaded(context),
    );

    return const Scaffold(
      body: Center(
        child: Text('Book Discovery Test App'),
      ),
    );
  }

  static Future<void> _onLoaded(BuildContext context) async {
    BlocProvider.of<AuthenticationBloc>(context)
        .add(AuthenticationStartEvent());
  }
}
