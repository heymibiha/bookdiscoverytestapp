import 'package:bookdiscoverytestapp/bloc/user_auth/user_auth_state.dart';
import 'package:flutter/material.dart';

typedef AuthSubmitCallback = void Function(
  String username,
  String password,
);

class AuthControl extends StatefulWidget {
  const AuthControl({
    Key? key,
    required this.onSubmit,
    required this.submitButtonName,
    required this.state,
  }) : super(key: key);

  final UserAuthState state;

  final AuthSubmitCallback onSubmit;

  final String submitButtonName;

  @override
  State<AuthControl> createState() => _AuthControlState();
}

class _AuthControlState extends State<AuthControl> {
  final TextEditingController _usernameTextController = TextEditingController();
  final TextEditingController _passwordTextController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    final bool activeControls = widget.state is! UserAuthInProgressState;

    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        _buildUsernameField(enabled: activeControls),
        const SizedBox(height: 40),
        _buildPwdField(enabled: activeControls),
        const SizedBox(height: 40),
        _buildSubmitButton(enabled: activeControls),
      ],
    );
  }

  Widget _buildUsernameField({required bool enabled}) {
    return TextField(
      controller: _usernameTextController,
      enabled: enabled,
      decoration: const InputDecoration(
        labelText: 'username',
      ),
    );
  }

  Widget _buildPwdField({required bool enabled}) {
    return TextField(
      controller: _passwordTextController,
      enabled: enabled,
      obscureText: true,
      decoration: const InputDecoration(
        labelText: 'password',
      ),
    );
  }

  @override
  void dispose() {
    _usernameTextController.dispose();
    _passwordTextController.dispose();
    super.dispose();
  }

  Widget _buildSubmitButton({required bool enabled}) {
    return ElevatedButton(
      onPressed: enabled ? _onSubmitButtonPressed : null,
      child: enabled
          ? Text(widget.submitButtonName)
          : const SizedBox(
              width: 20,
              height: 20,
              child: CircularProgressIndicator(),
            ),
    );
  }

  void _onSubmitButtonPressed() {
    widget.onSubmit.call(
      _usernameTextController.text,
      _passwordTextController.text,
    );
  }
}
