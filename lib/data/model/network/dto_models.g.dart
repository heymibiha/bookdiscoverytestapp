// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'dto_models.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

LoginDto _$LoginDtoFromJson(Map<String, dynamic> json) => LoginDto(
      json['username'] as String,
      json['password'] as String,
    );

Map<String, dynamic> _$LoginDtoToJson(LoginDto instance) => <String, dynamic>{
      'username': instance.username,
      'password': instance.password,
    };

UserDto _$UserDtoFromJson(Map<String, dynamic> json) => UserDto(
      json['id'] as String,
      json['username'] as String,
      json['token'] as String,
    );

Map<String, dynamic> _$UserDtoToJson(UserDto instance) => <String, dynamic>{
      'id': instance.id,
      'username': instance.username,
      'token': instance.token,
    };

BookDto _$BookDtoFromJson(Map<String, dynamic> json) => BookDto(
      json['id'] as String,
      json['title'],
      json['author'] as String,
      json['coverImageUrl'] as String?,
      json['pageCount'] as int,
      json['publisher'] as String?,
      json['synopsis'] as String?,
    );

Map<String, dynamic> _$BookDtoToJson(BookDto instance) => <String, dynamic>{
      'id': instance.id,
      'title': instance.title,
      'author': instance.author,
      'coverImageUrl': instance.coverImageUrl,
      'pageCount': instance.pageCount,
      'publisher': instance.publisher,
      'synopsis': instance.synopsis,
    };
