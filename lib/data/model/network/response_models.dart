import 'package:bookdiscoverytestapp/data/model/network/dto_models.dart';
import 'package:json_annotation/json_annotation.dart';

part 'response_models.g.dart';

@JsonSerializable()
class MessageResponse {
  MessageResponse(this.message);

  factory MessageResponse.fromJson(Map<String, dynamic> json) =>
      _$MessageResponseFromJson(json);

  final String? message;
}

@JsonSerializable()
class UserDtoResponse {
  UserDtoResponse(this.user);

  factory UserDtoResponse.fromJson(Map<String, dynamic> json) =>
      _$UserDtoResponseFromJson(json);

  final UserDto user;
}

@JsonSerializable()
class BookListDtoResponse {
  BookListDtoResponse(this.books);

  factory BookListDtoResponse.fromJson(Map<String, dynamic> json) =>
      _$BookListDtoResponseFromJson(json);

  final List<BookDto> books;
}

@JsonSerializable()
class BookDtoResponse {
  BookDtoResponse(this.book);

  factory BookDtoResponse.fromJson(Map<String, dynamic> json) =>
      _$BookDtoResponseFromJson(json);

  final BookDto book;
}
