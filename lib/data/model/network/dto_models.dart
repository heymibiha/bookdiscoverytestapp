import 'package:json_annotation/json_annotation.dart';

part 'dto_models.g.dart';

@JsonSerializable()
class LoginDto {
  LoginDto(this.username, this.password);

  final String username;
  final String password;

  Map<String, dynamic> toJson() => _$LoginDtoToJson(this);
}

abstract class BaseIdDto {
  BaseIdDto(this.id);

  final String id;
}

@JsonSerializable()
class UserDto extends BaseIdDto {
  UserDto(String id, this.username, this.token) : super(id);

  factory UserDto.fromJson(Map<String, dynamic> json) =>
      _$UserDtoFromJson(json);

  final String username;
  final String token;
}

@JsonSerializable()
class BookDto extends BaseIdDto {
  BookDto(
    String id,
    this.title,
    this.author,
    this.coverImageUrl,
    this.pageCount,
    this.publisher,
    this.synopsis,
  ) : super(id);

  factory BookDto.fromJson(Map<String, dynamic> json) =>
      _$BookDtoFromJson(json);

  final dynamic title;
  final String author;
  final String? coverImageUrl;
  final int pageCount;
  final String? publisher;
  final String? synopsis;
}
