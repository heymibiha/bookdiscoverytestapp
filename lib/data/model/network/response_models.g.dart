// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'response_models.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

MessageResponse _$MessageResponseFromJson(Map<String, dynamic> json) =>
    MessageResponse(
      json['message'] as String?,
    );

Map<String, dynamic> _$MessageResponseToJson(MessageResponse instance) =>
    <String, dynamic>{
      'message': instance.message,
    };

UserDtoResponse _$UserDtoResponseFromJson(Map<String, dynamic> json) =>
    UserDtoResponse(
      UserDto.fromJson(json['user'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$UserDtoResponseToJson(UserDtoResponse instance) =>
    <String, dynamic>{
      'user': instance.user,
    };

BookListDtoResponse _$BookListDtoResponseFromJson(Map<String, dynamic> json) =>
    BookListDtoResponse(
      (json['books'] as List<dynamic>)
          .map((e) => BookDto.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$BookListDtoResponseToJson(
        BookListDtoResponse instance) =>
    <String, dynamic>{
      'books': instance.books,
    };

BookDtoResponse _$BookDtoResponseFromJson(Map<String, dynamic> json) =>
    BookDtoResponse(
      BookDto.fromJson(json['book'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$BookDtoResponseToJson(BookDtoResponse instance) =>
    <String, dynamic>{
      'book': instance.book,
    };
