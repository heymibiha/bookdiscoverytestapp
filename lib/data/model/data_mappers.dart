import 'package:bookdiscoverytestapp/data/model/domain/domain_models.dart';
import 'package:bookdiscoverytestapp/data/model/network/dto_models.dart';
import 'package:dio/dio.dart';

class DataMapper {
  DataMapper._();

  static BookListItemDomain convertBookDtoToListItemDomain(BookDto dto) =>
      BookListItemDomain(
        id: dto.id,
        title: _dynamicTitleToString(dto.title),
        coverImageUrl: dto.coverImageUrl,
      );

  static BookDomain convertBookDtoToDomain(BookDto dto) => BookDomain(
        id: dto.id,
        title: _dynamicTitleToString(dto.title),
        coverImageUrl: dto.coverImageUrl,
        author: dto.author,
        pageCount: dto.pageCount,
        publisher: dto.publisher,
        synopsis: dto.synopsis,
      );

  static String _dynamicTitleToString(dynamic title) {
    if (title is String) {
      return title;
    }

    return '$title';
  }

  static Exception? dynamicErrorToTyped(dynamic error) {
    dynamic ptr = error;

    if (ptr is DioError) {
      ptr = ptr.error;
    }

    if (ptr is Exception) {
      return ptr;
    }

    return null;
  }
}
