abstract class BaseIdDomain {
  BaseIdDomain({required this.id});

  final String id;
}

class BookListItemDomain extends BaseIdDomain {
  BookListItemDomain({
    required String id,
    required this.title,
    this.coverImageUrl,
  }) : super(id: id);

  final String title;
  final String? coverImageUrl;
}

class BookDomain extends BookListItemDomain {
  BookDomain({
    required String id,
    required String title,
    required this.author,
    String? coverImageUrl,
    required this.pageCount,
    this.publisher,
    this.synopsis,
  }) : super(
          id: id,
          title: title,
          coverImageUrl: coverImageUrl,
        );

  final String author;
  final int pageCount;
  final String? publisher;
  final String? synopsis;
}
