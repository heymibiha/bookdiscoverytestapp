class UnAuthException implements Exception {}

class MessageException implements Exception {
  MessageException(this.message);

  final String? message;
}
