import 'package:bookdiscoverytestapp/data/model/network/dto_models.dart';

abstract class DataSource {
  Future<UserDto> login(LoginDto loginDto);
  Future<UserDto> register(LoginDto loginDto);

  Future<List<BookDto>> books(String? query);
  Future<BookDto> book(String id);
}
