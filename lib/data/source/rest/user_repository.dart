import 'dart:async';

import 'package:shared_preferences/shared_preferences.dart';

typedef TokenChangeCallback = void Function(String? token);

class UserRepository {
  UserRepository({this.tokenChangeCallback});

  final TokenChangeCallback? tokenChangeCallback;

  static const String TOKEN_PREF_KEY = 'auth_token';

  Future<void> deleteToken() async {
    (await _sharedPreferences).remove(TOKEN_PREF_KEY);
    tokenChangeCallback?.call(null);
  }

  Future<void> saveToken(String token) async {
    (await _sharedPreferences).setString(TOKEN_PREF_KEY, token);
    tokenChangeCallback?.call(token);
  }

  Future<String?> loadToken() async {
    final String? token = (await _sharedPreferences).getString(TOKEN_PREF_KEY);
    tokenChangeCallback?.call(token);
    return token;
  }

  Future<SharedPreferences> get _sharedPreferences =>
      SharedPreferences.getInstance();
}
