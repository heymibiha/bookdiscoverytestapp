import 'package:bookdiscoverytestapp/data/model/network/dto_models.dart';
import 'package:bookdiscoverytestapp/data/source/data_source.dart';
import 'package:bookdiscoverytestapp/data/source/rest/http_client.dart';
import 'package:bookdiscoverytestapp/data/source/rest/rest_client.dart';

class RestDataSource extends DataSource {
  RestDataSource(HttpClient httpClient) : _restClient = RestClient(httpClient.dio);

  final RestClient _restClient;

  @override
  Future<UserDto> login(LoginDto loginDto) async =>
      (await _restClient.login(loginDto)).user;

  @override
  Future<UserDto> register(LoginDto loginDto) async =>
      (await _restClient.register(loginDto)).user;

  @override
  Future<List<BookDto>> books(String? query) async =>
      (await _restClient.books(query)).books;

  @override
  Future<BookDto> book(String id) async => (await _restClient.book(id)).book;
}
