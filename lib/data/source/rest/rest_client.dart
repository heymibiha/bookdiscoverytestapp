import 'package:bookdiscoverytestapp/data/model/network/dto_models.dart';
import 'package:bookdiscoverytestapp/data/model/network/response_models.dart';
import 'package:dio/dio.dart';
import 'package:retrofit/retrofit.dart';

part 'rest_client.g.dart';

@RestApi(baseUrl: 'http://104.248.26.141:3000/api/')
abstract class RestClient {
  factory RestClient(Dio dio, {String? baseUrl}) = _RestClient;

  @POST('auth/login')
  Future<UserDtoResponse> login(@Body() LoginDto loginModel);

  @POST('auth/register')
  Future<UserDtoResponse> register(@Body() LoginDto loginModel);

  @GET('books')
  Future<BookListDtoResponse> books(@Query('q') String? query);

  @GET('books/{id}')
  Future<BookDtoResponse> book(@Path('id') String id);
}
