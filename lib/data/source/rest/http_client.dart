import 'package:bookdiscoverytestapp/data/model/error/exceptions.dart';
import 'package:bookdiscoverytestapp/data/model/network/response_models.dart';
import 'package:dio/dio.dart';

class HttpClient {
  HttpClient() {
    _addErrorInterceptors();
  }

  final Dio dio = Dio();

  static const String BEARER_TOKEN_HEADER = 'Authorization';
  static const String BEARER_TOKEN_PREFIX = 'Bearer';

  set refreshToken(String? token) {
    if (token?.isEmpty ?? true) {
      dio.options.headers.remove(BEARER_TOKEN_HEADER);
    } else {
      dio.options.headers[BEARER_TOKEN_HEADER] = '$BEARER_TOKEN_PREFIX $token';
    }
  }

  void _addErrorInterceptors() {
    final Interceptor interceptor = InterceptorsWrapper(
      onRequest:
          (RequestOptions requestOptions, RequestInterceptorHandler handler) {
        handler.next(requestOptions);
      },
      onResponse: (Response response, ResponseInterceptorHandler handler) {
        handler.next(response);
      },
      onError: (
        DioError error,
        ErrorInterceptorHandler handler,
      ) {
        if (error.response?.statusCode == 401) {
          throw UnAuthException();
        } else if (error.response?.statusCode == 400) {
          final MessageResponse messageResponse = MessageResponse.fromJson(
            error.response?.data as Map<String, dynamic>,
          );
          throw MessageException(messageResponse.message);
        } else {
          handler.next(error);
        }
      },
    );

    dio.interceptors.add(interceptor);
  }
}
