### License
All Rights reserved. Any modifications and code usage are denied. Only for viewing. 

### How to run
1. Clone the project.
2. Make sure your Flutter and Dart SDK are installed properly.
3. Make sure you have installed Android/iOS build tools.   
4. Connect your Android/iOS device or Emulator and allow debugging. 
5. Run the app by calling 'flutter run' command from the root directory of this project.

### How to distribute (short instruction)
1. Prepare Google/Apple dev accounts
2. Add signing config to the app.
3. Build the app binary in the release mode.
4. Upload the app binary to your dev account and publish it to the market.
